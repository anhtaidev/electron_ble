const $ = jQuery = require('jquery');
const path = require('path');
const fs = require('fs');
const { app, BrowserWindow, ipcMain } = require('electron')


var spawn = require('child_process').spawn;

function createWindow () {
    const win = new BrowserWindow({
      width: 800,
      height: 600,
      webPreferences: {
        nodeIntegration: true,
        preload: path.join(__dirname, "preload.js")
      }
    })
    
    win.loadFile('./src/pages/home/home.html');

    app.commandLine.appendSwitch('enable-experimental-web-platform-features')

    console.log("this ok");
    win.webContents.on('select-bluetooth-device', (event, deviceList, callback) => {
      console.log(deviceList);
      event.preventDefault()
      const result = deviceList.find((device) => {
        return true;
      })
      if (!result) {
        callback('')
      } else {
        callback(result.deviceId)
      }
    })

    //handles scanning event
    ipcMain.on("scanningBle", (event, args) => {

      //call python lib to scanning ble
      let process = spawn('python', ['./src/libs/python/scanBle.py']);
      process.stdout.on('data', function(data) {
        devices = data.toString();

        //sent scanning result to view
        win.webContents.send("scanningResult", {devices: JSON.parse(devices)});
      });
    });
}

app.whenReady().then(() => {
    createWindow();
})
