function sendToMain() {
    window.api.send("scanningBle", "some data");
}

var devices = [];
$( document ).ready(function() {
    $("#send-btn").on("click", sendToMain);

    window.api.receive("scanningResult", (data) => {
        devices = data.devices;
        renderDevices();
    });
});

function renderDevices() {
    $("#table-device").empty();
    for(device of devices) {
        let tag = `
            <tr>
                <td>${device.address}</td>
                <td>${device.name}</td>
                <td>
                    <button class="btn btn-success" onclick="connectBleDevice('${device.name}','${device.metadata.uuids[0]}')">
                        Connect
                    </button>
                </td>
            </tr>
        `;
        $("#table-device").append(tag);
    }
}

var _device;
function connectBleDevice(name, service_id) {
    let options = {
        filters: [
            // {services: [service_id]},
            {name: name}
        ]
    };
    navigator.bluetooth.requestDevice(options)
    .then((device) => {
        _device = device;
        return device.gatt.connect();
    })
    .then((server) => {
        this.server = server;
        console.log('Getting Service...');
        return server.getPrimaryService(serviceUuid);
    })
    .then((service) => {
        this.service = service;
        console.log('Getting Characteristics...');
        return service.getCharacteristics();
    })
    .then((characteristics) => {
        this.characteristics = characteristics;
        console.log(characteristics);
        return characteristics;
    })
}