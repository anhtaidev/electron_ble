import asyncio
import json
from bleak import BleakScanner

async def run():
    devices = await BleakScanner.discover()
    realDevice = []
    for d in devices:
        if(d.name):
            realDevice.append({
                "address": d.address,
                "name": d.name,
                "metadata": d.metadata
            });
    print(json.dumps(realDevice));
loop = asyncio.get_event_loop()
loop.run_until_complete(run())